const express = require('express');
const cors = require('cors');
const router = require('./routes');
const userRouter = require('./routes/user')
const app = express();

app.use(express.json())
app.use(cors())

// entry point
app.use('/api/v1/', router)

app.get('/', (req, res) => {
    res.send("Server is currently on development")
})


module.exports = app;
