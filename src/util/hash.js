const bcrypt = require('bcrypt');
const { User } = require('../models')
const { config } = require('../config/app')

const register = async (params) => {
    const newUser = params;
    const saltRounds = 8
    const genSalt = await bcrypt.genSalt(saltRounds)
    newUser.password = await bcrypt.hash(newUser.password, genSalt)
    return newUser
}

const authenticate = async ({email, password}) => {
    const account = await User.findOne({
        where: { email }
    })
    if (!account) {
        throw new Error('Unable to login')
    }
    const isMatch = await bcrypt.compare(password, account.password)
    if (!isMatch) {
        throw new Error('Unable to login')
    }
    return account
}

module.exports = { 
    register,
    authenticate
}
