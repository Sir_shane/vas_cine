const jwt = require('jsonwebtoken')
const { config } = require('../config/app')
const { User, authToken } = require('../models')

const auth = async (req, res, next) => { 
    try {

       const token = req.headers.authorization.split(" ")[1];
       const decoded = jwt.verify(token, config.app.secret)
       const user = await User.findOne({ 
           where: {
               id: decoded.id
           }
       })
       if(!user) {
           throw new Error()
       }
       req.user = user
       next()

    } catch (error) {
        res.status(401).send('Unauthorized')
    }
}

const auth2 = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, config.app.secret);
    const user = await User.findOne({
      where: {
        id: decoded.id,
      },
    });
    if (user.role.toLowerCase() !== 'admin') {
      throw new Error();
    }
    req.user = user;
    next();
  } catch (error) {
    res.status(401).send("Unauthorized");
  }
};

module.exports = {auth, auth2}
