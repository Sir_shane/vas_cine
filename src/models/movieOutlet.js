'use strict'
const { Model } = require('sequelize')

module.exports = (sequelize, DataTypes) => {
    class MovieOutlet extends Model {
        static associate(models) {

        }
    };
    MovieOutlet.init(
      {
        movie_name: DataTypes.STRING,
        movie_description: DataTypes.STRING,
        movie_duration: DataTypes.STRING,
        movie_genre: DataTypes.STRING,
        location: DataTypes.STRING,
        show_start: DataTypes.STRING,
        show_end: DataTypes.STRING,
      },
      {
        sequelize,
        modelName: "MovieOutlet",
      }
    );

    return MovieOutlet
}
