'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.authToken);
    }
  };
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: {
      type:DataTypes.STRING,
      allowNull: false,
      unique: true, 
      isEmail: true
    },
    password: {
      type:DataTypes.STRING,
      allowNull: false
    },
    role: {
      type:DataTypes.STRING,
      defaultValue: "user"
    }
  }, {
    sequelize,
    modelName: 'User',
  });

  User.authorise = async function () {
    const { authToken } = sequelize.models;
    const user = this;

    console.log(user.id);
    const token = await authToken.generate(user.id);

    return { user, token };
  };

  User.logout = async function (token) {
    sequelize.models.authToken.destroy({
      where: {
        token
      }
    })
  }

  return User;
};