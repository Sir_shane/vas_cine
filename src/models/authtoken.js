'use strict';
const jwt = require("jsonwebtoken");
const { config } = require('../config/app')

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class authToken extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      authToken.belongsTo(models.User)
    }
  };
  
  authToken.init(
    {
      token: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "authToken",
    }
  );

  authToken.generate = async (UserId) => {
    if (!UserId) {
      throw new Error('AuthToken requires a userId')
    }

    const token = await jwt.sign({ id: UserId }, config.app.secret)

    return await authToken.create({token, UserId})
  }

  return authToken;
};