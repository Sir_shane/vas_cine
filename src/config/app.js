const config = {
  app: {
    port: process.env.PORT,
    salt: process.env.SALT,
    secret: process.env.SECRET
  },
  db: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    dialect: process.env.DB_CONNECTION,
    port: process.env.DB_PORT,
  },
};

module.exports = {
    config
}