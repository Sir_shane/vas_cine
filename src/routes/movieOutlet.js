const express = require('express');
const { newShow, displayAll, displayAllByOutlet, deleteShow } = require('../controller/outlet');
const { auth2 } = require('../middleware/auth');
const router = express.Router();

router.post('/newShow', auth2, newShow)

router.get('/all', displayAll);

router.get('/location', displayAllByOutlet)

router.delete('/show/:id', auth2 ,deleteShow)

module.exports = router;
