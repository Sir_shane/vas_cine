const express = require('express');
const { creatNewUser, updateProfile, fetchAllUsers, profile } = require('../controller/user');
const { auth } = require('../middleware/auth');

const router = express.Router();

router.post('/create', creatNewUser);

// coming soon
// router.post('/login')

// router.post('/logout')

router.patch('/edit/:id', updateProfile)

router.get("/me", auth, profile);

router.get("/", auth, fetchAllUsers);


module.exports = router