const router = require('express').Router();
const userRouter = require('./user');
const outletRouter = require('./movieOutlet');

router.use('/user', userRouter)
router.use('/outlet', outletRouter)

module.exports = router