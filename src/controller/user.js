const { User } = require('../models');
const { register } = require('../util/hash')

const creatNewUser = async (req, res) => {
    const user = await register(req.body);
    try { 
        const newUser = await User.create({...user});
        const data = await newUser.authorise();
        res.status(201).send(data)
    } catch (error) {
        console.log(error)
       res.status(400).send(error) 
    }
}

// handle laterau
const login = async (req, res) => {
    try {

    } catch (error) {
        res.status(400).send(error.message)
    }
}

const fetchAllUsers = async (req, res) => {
    const fetchUsers = await User.findAll();
    res.status(200).send(fetchUsers)
}

const profile = async (req, res) => {
    res.status(200).send(req.user)
}

const updateProfile = async(req, res) => {
    const allowedUpdates = ['password']
    const updates = Object.keys(req.body)
    const isValid = updates.every(update => update.includes(allowedUpdates));
    const profileId = req.params.id;
    const userProfile = await User.findOne({
        where: {
            id: profileId
        }
    })

    if (!isValid) {
        return res.status(400).send("invalid update")
    }

    try {
        const  finalUpdate = await userProfile.update(req.body);
        res.status(200).send(finalUpdate)
        
    } catch (error) {
        res.status(500).send(error.message)
    }
    res.send("Hello world")
}

module.exports = {
    creatNewUser,
    updateProfile,
    fetchAllUsers,
    profile
}