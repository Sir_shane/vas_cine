const { MovieOutlet }  = require('../models')

const newShow = async (req, res) => {
    const payload = req.body;
    try {
        const movie = await MovieOutlet.create({...payload});
        res.status(200).send(movie)
    } catch (error) {
        res.status(400).send({})
    }
}

const displayAll = async (req, res) => {
    const fetchAllMovies = await MovieOutlet.findAll();
    res.status(200).send(fetchAllMovies)
}

const displayAllByOutlet = async (req, res) => {
    const location = req.body.location.toLowerCase();
    const outlets = await MovieOutlet.findAll({
        where: { 
            location
        }
    })
    console.log(outlets)
}

const deleteShow = async (req, res) => {
    const id = req.params.id;
    const deleted = await MovieOutlet.destroy({
        where: { id }
    })
    res.status(200).send(deleted)
};

module.exports = {
    newShow,
    displayAll,
    displayAllByOutlet,
    deleteShow
}