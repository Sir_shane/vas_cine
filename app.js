// const sequelize = require("/models");
const app = require("./src/server");
const { config } = require("./src/config/app")
const { sequelize } = require('./src/models')

const PORT = config.app.port;

// const jwt = require('jsonwebtoken');

// async function generateToken(arg) {
//   const token = jwt.sign({
//     id: arg
//   }, 'helloworld')
//   console.log(token)
// }



app.listen(PORT, async () => {
  console.log(`server running on Port ${PORT}`);
  await sequelize.sync();
  console.log('Database Connected')
})

